#!/bin/bash
set -e

if [ -z "$1" ]
then
echo "Please specify a root URL of the website"
else
#  Ajout d'une temporisation 
sleep 5s

##Count reset to 0
##curl $1/reset

##Send ping ten times
for i in `seq 1 10`;
do
  curl $1/ping -s -m 2 2>1 >/dev/null
done

##Saves the response into a file
curl $1/pong | tee pong.txt

##Compares with file with the expected result
if diff "pong.txt" "result.txt" &> /dev/null ; then
  echo "Everything's working\n" > /log/pingpong.log 
else
  echo "Something's wrong"
   exit 1
fi

exit 0
fi
